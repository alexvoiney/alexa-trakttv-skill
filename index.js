/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/

'use strict';
const Alexa = require('alexa-sdk');
const https = require('https');

const APP_ID = '';

const SKILL_NAME = 'TrackT';
const HELP_REPROMPT = 'En quoi puis-je vous aider ?';
const HELP_MESSAGE = 'Demandez moi le ce que vous avez regardé en dernier ou de vous conseiller un film...' + HELP_REPROMPT;
const STOP_MESSAGE = 'Au revoir !';

let USER_TOKEN = '';
const BASE_TRAKT_URL = 'api.trakt.tv';

//=========================================================================================================================================
//Editing anything below this line might break your skill.
//=========================================================================================================================================

function httpGet(query, callback) {
    let options = {
        host: BASE_TRAKT_URL,
        path: query,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'trakt-api-version': 2,
            'trakt-api-key': '',
            'Authorization': 'Bearer ' + USER_TOKEN
        },
    };

    let req = https.request(options, res => {
        res.setEncoding('utf8');
        const { statusCode } = res;
        let error = false;
        let responseString = "";
        
        if (statusCode != 200) {
            error = true;
        }
        
        //accept incoming data asynchronously
        res.on('data', chunk => {
            responseString = responseString + chunk;
        });
        
        //return the data when streaming is complete
        res.on('end', () => {
            try {
                    callback({
                    success: !error,
                    statusCode: statusCode,
                    message: error ? 'Pardonnez-moi, mais je n\'arrive à vous connecter à vous connecter à Trakt TV.' : 'Connection réussie !',
                    response: JSON.parse(responseString)
                });
            } catch (e) {
                callback({
                    success: false,
                    statusCode: statusCode,
                    result: e.message,
                    response: responseString
                });
            }
        });
    });
    req.end();
}


const handlers = {
    'LaunchRequest': function () {
        this.response.speak('Bienvenu sur Trak.tv. Que puis-je faire pour vous ?').listen('Que puis-je faire pour vous ?');
        this.emit(':responseReady');
    },
    'HintMovieIntent': function () {
        let query = '/users/me/collection/movies';
        httpGet(query, (collectionResult) => {
            if (collectionResult.success) {
                let randMovie = collectionResult.response[Math.floor(Math.random() * collectionResult.response.length)].movie;
                
                let titleQuery = '/movies/' + randMovie.ids.slug + '/translations/fr';
                httpGet(titleQuery, (theResult) => {
                    if (theResult.success) {
                        this.response.speak('Pourquoi ne pas essayer : ' + theResult.response[0].title + '.');
                    } else {
                        this.response.speak(theResult.message || 'Veuillez m\'excusez, une erreur inconnue s\'est produite.');
                    }
                    this.emit(':responseReady');
                });
                
            } else {
                this.response.speak(collectionResult.message || 'Veuillez m\'excusez, une erreur inconnue s\'est produite.');
                this.emit(':responseReady');
            }
        });
    },
    'TrendingMovieIntent': function () {
        let query = '/movies/trending';
        httpGet(query, (collectionResult) => {
            if (collectionResult.success) {
                let randMovie = collectionResult.response[Math.floor(Math.random() * collectionResult.response.length)].movie;
                
                let titleQuery = '/movies/' + randMovie.ids.slug + '/translations/fr';
                httpGet(titleQuery, (theResult) => {
                    if (theResult.success) {
                        this.response.speak('Pourquoi ne pas essayer : ' + theResult.response[0].title + '.');
                    } else {
                        this.response.speak(theResult.message || 'Veuillez m\'excusez, une erreur inconnue s\'est produite.');
                    }
                    this.emit(':responseReady');
                });
                
            } else {
                this.response.speak(collectionResult.message || 'Veuillez m\'excusez, une erreur inconnue s\'est produite.');
                this.emit(':responseReady');
            }
        });
    },
    'AMAZON.HelpIntent': function () {
        let query = '/users/me';
        httpGet(query, (theResult) => {
            if (theResult.success) {
                let speechOutput = HELP_MESSAGE;
                this.response.speak(speechOutput).listen(HELP_REPROMPT);
            } else {
                this.response.speak(theResult.message || 'Veuillez m\'excusez, une erreur inconnue s\'est produite.');
            }
            this.emit(':responseReady');
        });
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
};

exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    USER_TOKEN = event.context.System.user.accessToken;
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};
