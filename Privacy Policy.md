# Politique de confidentialité

L'utilisation de cette skill Alexa nécessite de connecter votre compte Trakt.tv à votre compte Amazon.   
En aucun cas nous n'utilisons ni ne revendons les données qui vous identifie sur ce service.

La seule donnée collectée et utilisée par cette skill Alexa est la liste des films de votre collection Trakt.tv.  
Cette liste n'est conservée que le temps de la parcourir pour répondre à votre demande.  
En aucun cas cette liste n'est conservée, ni revendue ou donnée à un service tiers.

# Consentement
En utilisant notre skill Alexa, vous acceptez notre politique de confidentialité et acceptez ses termes et conditions.


# Révocation de l'accès
Pour révoquer l'accès de votre compte Trakt.tv par Amazon, accéder à la page des [applications authorisées](https://trakt.tv/oauth/authorized_applications) sur le service Trakt.tv.  
Révoquer l'accès à votre compte Trakt.tv empêchera l'utilisation de notre skill Alexa.
